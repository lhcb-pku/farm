计算集群使用
=============

北大LHCb组目前有一台服务器用于CPU计算，GPU计算和文件存储，安装的操作系统为 `CERN CentOS 7`_。

.. _CERN CentOS 7: https://linux.web.cern.ch/centos7/

集群介绍
-------------

+----------------+----------------+-----------------+----------------------------------------------+----------+-----------------------------+
| IP             | 系统           | 存储空间        | CPU                                          | 线程数   | GPU                         |
+================+================+=================+==============================================+==========+=============================+
| 162.105.151.42 | CERN CentOS 7  | ``/home`` 1.7T  | Intel(R) Xeon(R) Platinum 8276 CPU @ 2.20GHz | 224      | 2 * NVIDIA GeForce RTX 3090 |
+                +                +                 +                                              +          +                             +
|                |                | ``/data`` 6.6T  |                                              |          |                             |
+                +                +                 +                                              +          +                             +
|                |                | ``/data2`` 8.7T |                                              |          |                             |
+----------------+----------------+-----------------+----------------------------------------------+----------+-----------------------------+

在计算集群本地安装了 `ROOT`_ 和 `Geant4`_ 等数据分析需要的软件。
用户的home目录在 ``/home/username`` 下，但系统盘空间有限，因此禁止在个人主目录下占用过多空间。
工作目录位于 ``/data*/lhcb`` 下，目前有两块磁盘共15.3TB的存储空间，
初次使用的用户需要在此目录下建立自己的文件夹。本地软件安装在 ``/data/soft`` 下。

.. _ROOT: https://root.cern.ch/
.. _Geant4: https://geant4.web.cern.ch/node/1 

登录方式
-------------

在北大校园网环境下（在校内或在校外使用VPN），直接连接网络中心分配的IP地址即可登录


.. code-block:: bash

    > ssh -Y username@162.105.151.42


其中 ``username`` 是计算集群上的用户名。 ``-Y`` 指令开启服务器到本地的X11传输，让服务器端的图形能够显示。

LHCb软件使用
-------------

计算集群已安装 `CVMFS`_ 服务，可以直接调用LHCb的软件库。
使用时输入 ``setenv_lhcb_lxplus``，之后即可按照lxplus环境下进行使用。
关于lxplus环境下LHCb软件的使用可以参考 `Starterkit`_。

.. _CVMFS: https://cvmfs.readthedocs.io/en/stable/
.. _Starterkit: https://lhcb.github.io/starterkit-lessons/first-analysis-steps/README.html

集群上安装了Kerberos服务，用户可以通过Kerberos来访问CERN的afs等服务。
首先使用 ``ktutil`` 产生Kerberos密钥：

.. code-block:: bash

    > ktutil
    ktutil: addent -password -p <username>@CERN.CH -k 1 -e aes256-cts
    ktutil: addent -password -p <username>@CERN.CH -k 1 -e arcfour-hmac-md5
    ktutil: wkt .keytab
    ktutil: q

这样Kerberos密钥就被写入了 ``.keytab`` 文件，同样可以通过 ``ktutil`` 来检查密钥：

.. code-block:: bash

    > ktutil
    ktutil: rkt .keytab
    ktutil: list
    slot KVNO Principal
    ---- ---- ---------------------------------------------------------------------
       1    1                         <username>@CERN.CH
       2    1                         <username>@CERN.CH

这样Kerberos服务就配置好了，启用Kerberos则需要

.. code-block:: bash

    > kinit -kt .keytab username

如果没有报错则说明连接成功。

集群上也安装了afs client服务，通过Kerberos连接CERN网络后使用

.. code-block:: bash

    > aklog

即可访问afs，具体目录为 ``/afs/cern.ch/(user, work)/<u>/<username>``

启用Kerberos服务后，在 ``~/.ssh/config`` 中加入以下语句即可免密登陆lxplus:

.. code-block:: bash

    Host *
        GSSAPIAuthentication yes 
        GSSAPIDelegateCredentials yes


本地软件使用
-------------


目前本地安装了 `ROOT`_、 `Geant4`_ 、 `CUDA`_ 和 `Tensorflow`_ 等常用软件。
其中部分软件可以通过 ``lbrun`` 命令直接配置（与LHCb软件的 ``lb-run`` 区分开），使用方法为

.. _CUDA: https://developer.nvidia.com/cuda-toolkit
.. _Tensorflow: https://www.tensorflow.org/

.. code-block:: bash

    > lbrun [software] <version>

其中 ``[software]`` 是必须指定的软件，输入 ``lbrun list`` 可以列出目前已安装的软件：

.. code-block:: bash

    > lbrun list
    Available softwares: gcc root geant4

``<version>`` 是可选项，指需要配置的软件版本， 如果不输入版本则会使用最新版本。
使用 ``lbrun <software> list`` 会列出该软件所有可用版本：

.. code-block:: bash

    > lbrun ROOT list 
    Software: root 
    Available versions: 6.18.04 6.18.04-py3

使用 ``lbrun`` 配置软件环境，会自动配置该软件所需的前置软件的环境：
例如ROOT 6.18.04是基于gcc 8.3.1编译的，则使用 ``lbrun root 6.18.04`` 会将gcc 8.3.1的环境一并配置好。

目前LHCb计算集群上本地安装的软件及版本在下表中列出，其中gcc、ROOT和Geant4可以通过 ``lbrun`` 命令进行环境配置。
如有其他需求，可以联系管理员张舒楠（shunan@pku.edu.cn）。

+----------------+----------------------------------+-------------------------------------------------------+
| 软件名         | 版本                             | 路径                                                  |
+================+==================================+=======================================================+
| gcc            | 8.3.1                            | ``/opt/rh/devtoolset-8``                              |
+----------------+----------------------------------+-------------------------------------------------------+
| ROOT           | 6.18.04 （python2兼容）          | ``/data/soft/install/root-6.18.04``                   |
+                +----------------------------------+-------------------------------------------------------+
|                | 6.18.04-py3 （python3兼容）      | ``/data/soft/install/root-6.18.04-py3``               |
+                +----------------------------------+-------------------------------------------------------+
|                | 6.22.08                          | ``/data/soft/install/root-6.22.08``                   |
+                +----------------------------------+-------------------------------------------------------+
|                | 6.24.00                          | ``/data/soft/install/root-6.24.00``                   |
+----------------+----------------------------------+-------------------------------------------------------+
| Geant4         | 10.06                            | ``/data/soft/install/geant4.10.06``                   |
+----------------+----------------------------------+-------------------------------------------------------+
| CUDA           | 11.0                             | ``/usr/local/cuda-11.0``                              |
+----------------+----------------------------------+-------------------------------------------------------+
| Tensorflow-GPU | 2.5.0.dev20201104 （python3兼容）| ``/usr/local/lib/python3.8/site-packages/tensorflow`` |
+----------------+----------------------------------+-------------------------------------------------------+